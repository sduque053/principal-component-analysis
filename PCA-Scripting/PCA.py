#!/usr/bin/env python3

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

from sklearn.preprocessing import StandardScaler

iris = pd.read_csv('iris.data',header=None)

iris.dropna(how='all',inplace=True)

x = iris.iloc[:,(iris.dtypes==(float or int)).array].values
y = iris.iloc[:,~(iris.dtypes==(float or int)).array].values

X = StandardScaler().fit_transform(x)

CovMat = np.cov(X.T)

EigVal, EigVect=np.linalg.eig(CovMat)

VarExplained=[(i/sum(EigVal))*100 for i in EigVal]
CumVarExplained=np.cumsum(VarExplained)

f=True
for n in range(CumVarExplained.size):
    if f==True and CumVarExplained[n]>=90:
        f=False
        nComponents=n+1

ProjMatrix = EigVect[:,:nComponents]

X_pca=X.dot(ProjMatrix)

if y[0].size>1:
    fig,axes=plt.subplots(y[0].size)

    for n in range(y[0].size):
        for tp in np.unique(y[:,n]):
            sns.scatterplot(X_pca[y[:,n]==tp,0],X_pca[y[:,n]==tp,1],ax=axes[n])
        axes[n].legend(np.unique(y[:,n]))
else:
    fig,axes=plt.subplots(y[0].size)

    for n in range(y[0].size):
        for tp in np.unique(y[:,n]):
            sns.scatterplot(X_pca[y[:,n]==tp,0],X_pca[y[:,n]==tp,1])
        plt.legend(np.unique(y[:,n]))


plt.show()
